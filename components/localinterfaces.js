/*
本文件获取本机的网络接口信息并存入数据库
*/
const os=require('os');
const _=require('underscore');
const db=require('../database/db');

module.exports=()=>{
	/*
	 * 获取本机的各个网络接口的信息并存入数据库
	 *
	 * 忽略lo和没有ipv4地址的网络接口
	 */

	var interfaces=os.networkInterfaces();
	interfaces=_.omit(interfaces,addressList=>{
		/*
		 * 删除本地回环端口
		 */
		return addressList.findIndex(address=>address.internal)!==-1;
	});
	interfaces=_.mapObject(interfaces,addressList=>{
		/*
		 * 仅选用IPv4地址
		 */
		return addressList.find(address=>address.family==='IPv4');
	});
	interfaces=_.pick(interfaces,value=>value);
	/*
	 * 把接口名添加到结果中并转为数组
	 */
	interfaces=_.values(_.mapObject(interfaces,(address,interfaceName)=>_.extend(address,{interface:interfaceName})));
	/*
	 * 仅提取address,netmask和interface信息
	 */
	interfaces=interfaces.map(each=>_.pick(each,'interface','address','netmask'));
	/*
	 * 保存至数据库
	 */
	db.interfaces.insert(interfaces);
}
