const sock=require('./socket');
const db=require('../database/db');

module.exports=()=>{
	/*
	 * 当sock判断为发现新的节点时
	 * 若是新节点，则生成了一条新的纪录，包含可被发现节点的IP地址和本机对应的网络接口名
	 * 若是老节点，则刷新节点中的纪录
	 */
	sock.on('nodeDiscovered',(iface,address)=>{
		if(!db.nodes.count({interface:iface,address:address})){
			db.nodes.insertOne({interface:iface,address:address,failure:0});
		}
		else{
			var nodeDoc=db.nodes.findOne({interface:iface,address:address});
			nodeDoc.failure=0;
			db.nodes.update(nodeDoc);
		}
	});
}
