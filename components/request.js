const sock=require('./socket');
const db=require('../database/db');
const config=require('../config/config');
const _=require('underscore');

var timer=null;

const startProcess=()=>{
	/*
	 * 周期性任务
	 * 首先将failure数加1，然后发送请求包
	 * 如果其他节点响应，说明节点在线，failure减1
	 * 节点failure数达到3时认为节点离线
	 */
	scheduleTask();
	timer=setInterval(scheduleTask,config.get('queryIntervalTime')*1000);
}

const stopProcess=()=>clearInterval(timer);

const scheduleTask=()=>{
	failureCountUpByOne(); /* 任务一：各节点failure数统一加一 */
	sendDiscoveryRequest(); /* 任务二：发送查询广播包*/
	clearNonResponsingNode(); /*任务三：清除已过期的纪录*/
}

const failureCountUpByOne=()=>{
	/* 将failure加1
	 * filterFunction: ()=>true 选择所有的记录
	 * updateFunction: 将记录的failure加一
	 */
	db.nodes.findAndUpdate(
		()=>true,
		doc=>{
			doc.failure++;
			return doc;
		}
	);
}

/*
 * 发送查询请求
 */
const sendDiscoveryRequest=()=>db.interfaces.find().forEach(doc=>sock.emit('startDiscovery',doc.interface));

/*
 * 清除被判为离线的节点的记录
 */
const clearNonResponsingNode=()=>{
	var offlineNodes=db.nodes.find({failure:{$gt:config.get('maxFailure')}});
	if(offlineNodes.length){
		offlineNodes.forEach(each=>db.nodes.remove(each));
	}
}

exports.startProcess=startProcess;
exports.stopProcess=stopProcess;
