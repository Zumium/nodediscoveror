/*
定义了本模块内公用的UDP Socket，负责发送、反射以及接收UDP包，以及声明了一些辅助函数
*/
const Promise=require('bluebird');
const dgram=require('dgram');
const config=require('../config/config');
const db=require('../database/db');

/*
 * 相应包类型
 * TYPE_REQUEST代表查询包，TYPE_RESPONSE代表响应包
 * 包结构：
 * {
 *   iface: 'eth1',
 *   type: TYPE_REQUEST | TYPE_RESPONSE
 * }
 */
const TYPE_REQUEST=0;
const TYPE_RESPONSE=1;

/*
 * 广播地址计算函数
 */
const getBroadcastAddr=iface=>{
	'use strict';
	var ifaceData=db.interfaces.findOne({interface:iface});
	var address=Buffer.alloc(1);
	var netmask=Buffer.alloc(1);
	var broadcast=Buffer.alloc(1);
	var addressRaw=ifaceData.address.split('.').map(num=>parseInt(num));
	var netmaskRaw=ifaceData.netmask.split('.').map(num=>parseInt(num));
	var broadcastAddr=[];
	for(let i=0;i<4;i++){
		address.writeUInt8(addressRaw[i],0);
		netmask.writeUInt8(netmaskRaw[i],0);
		broadcast.writeInt8(~netmask.readInt8(0)|address.readInt8(0),0);
		broadcastAddr.push(broadcast.readUInt8(0));
	}
	return broadcastAddr.map(num=>num.toString()).join('.');
}

const sock=dgram.createSocket({type:'udp4',reuseAddr:false},(msg,rinfo)=>{
	'use strict';
	/*
	 * message 事件监听函数
	 * 将各种到来的请求进行分流，触发相应的事件
	 *
	 * 首先检查是否接受到了自己发出的包，如果是则丢弃
	 * 如果是TYPE_REQUEST包，则修改包类型为TYPE_RESPONSE后发回源地址
	 * 如果是TYPE_RESPONSE包，则触发socket的nodeDiscovered事件
	 */
	if(db.interfaces.count({address:rinfo.address}))
		return;
	let receivedPacket=JSON.parse(msg);
	if(receivedPacket.type===TYPE_REQUEST){
		receivedPacket.type=TYPE_RESPONSE;
		sock.send(JSON.stringify(receivedPacket),rinfo.port,rinfo.address);
	}
	if(receivedPacket.type===TYPE_RESPONSE){
		sock.emit('nodeDiscovered',receivedPacket.iface,rinfo.address);
	}
});
/*
 * 开始节点发现过程
 */
sock.on('startDiscovery',iface=>{
	var pkt=JSON.stringify({iface:iface,type:TYPE_REQUEST});
	sock.send(pkt,0,Buffer.byteLength(pkt),config.get('udpPort'),getBroadcastAddr(iface))
});

sock.ready=()=>new Promise((resolve,reject)=>
	sock.bind(config.get('udpPort'),()=>{
		sock.setBroadcast(true);
		sock.setTTL(1);
		resolve();
	})
);

module.exports=sock;
