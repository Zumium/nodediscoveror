const express=require('express');
const _=require('underscore');
const db=require('../database/db');

var router=module.exports=express.Router();

router.get('/localinterfaces',(req,res)=>{
	res.json(db.interfaces.find().map(doc=>_.pick(doc,'interface','address','netmask')));
});

router.get('/nodes',(req,res)=>{
	res.json(db.nodes.find().map(doc=>_.pick(doc,'address','interface')));
});

router.delete('/',(req,res)=>{
	res.sendStatus(200);
	process.exit();
});
