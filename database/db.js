/*
使用了内存数据库lokijs作为临时存储
本文件中声明了各个数据库
*/
const loki=require('lokijs');

const db=new loki();
const interfaceCollection=db.addCollection('interfaces');
const neighborNodeCollection=db.addCollection('neighborNodes');

exports.db=db;
exports.interfaces=interfaceCollection;
exports.nodes=neighborNodeCollection;
