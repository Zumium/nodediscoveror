/*
app.js -- 主文件
	主文件是NodeDiscoveror的入口文件，
	负责启动模块内的各内部部分，
	以及启动HTTP服务器以及绑定监听地址
*/
const http=require('http');
const express=require('express');
const Promise=require('bluebird');
const config=require('./config/config');
const localInterfaces=require('./components/localinterfaces');
const response=require('./components/response');
const request=require('./components/request');
const router=require('./components/router');
const socket=require('./components/socket');
const sendready=require('./config/sendready');
const fs=Promise.promisifyAll(require('fs'));

const app=express();

Promise.resolve()
.then(()=>config.load())
.then(()=>socket.ready())
.then(()=>{
	localInterfaces();			//获取本地网络接口信息
	response();				//启动UDP接收器
	app.use(router);			//装载router
	request.startProcess();			//开始探测过程
	return fs.accessAsync(config.get('serverSocket'),fs.constants.F_OK);
})
.then(
	()=>fs.unlinkAsync(config.get('serverSocket')),
	()=>Promise.resolve()
)
.then(
	()=>app.listen(config.get('serverSocket'))
)
.then(
	()=>sendready('NodeDiscoveror')
);

//Does nothing when receiving SIGQUIT
process.on('SIGQUIT',()=>'');
